package com.example.videos;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;


import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;


public class MainActivity extends AppCompatActivity
{
    public Button btn_Local, btn_Remote, btn_pauseMusic, btn_playMusic, btn_playlocalvideo, btn_pauselocalvideo;
    public VideoView video_local;
    public MediaPlayer song;
    public YouTubePlayerView yp_remoteVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setVariables();
        setListeners();
        startMusic();
    }

    public void setVariables()
    {
        video_local = (VideoView) findViewById(R.id.video_local);
        yp_remoteVideo = (YouTubePlayerView) findViewById(R.id.yp_remote_video);
        setBtnVar();
        setLocalVideo();
    }

    public void setBtnVar()
    {
        btn_Local = (Button) findViewById(R.id.btn_local);
        btn_Remote = (Button) findViewById(R.id.btn_remote);
        btn_pauseMusic = (Button) findViewById(R.id.btn_pause_music);
        btn_playMusic = (Button) findViewById(R.id.btn_play_music);
        btn_playlocalvideo = (Button)findViewById(R.id.playlocalvideo);
        btn_pauselocalvideo = (Button)findViewById(R.id.pauselocalvideo);
    }

    public void setLocalVideo()
    {
        String path = "android.resource://" + getPackageName() + "/" + R.raw.ghetto;
        video_local.setVideoPath(path);
    }


    public void setListeners()
    {
        btn_Local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yp_remoteVideo.setVisibility(View.GONE);
                video_local.setVisibility(View.VISIBLE);
                video_local.start();
            }
        });

        btn_Remote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                video_local.setVisibility(View.GONE);
                video_local.stopPlayback();
                yp_remoteVideo.setVisibility(View.VISIBLE);
                remoteVideo();
            }
        });

        btn_pauseMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                song.pause();
            }
        });

        btn_playMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                song.start();
            }
        });

        btn_pauselocalvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                video_local.pause();
            }
        });

        btn_playlocalvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                video_local.start();
            }
        });

    }

    public void startMusic()
    {
        song = MediaPlayer.create(getApplicationContext(), R.raw.thunder);
        song.start();
    }

    public void remoteVideo()
    {
        getLifecycle().addObserver(yp_remoteVideo);
        yp_remoteVideo.getYouTubePlayerWhenReady(youTubePlayer -> {youTubePlayer.cueVideo("Zi_XLOBDo_Y",0);});
    }
}
